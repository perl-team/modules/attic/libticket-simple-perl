libticket-simple-perl (0.0.2-5) UNRELEASED; urgency=medium

  [ gregor herrmann ]
  * Remove Xavier Oswald from Uploaders. (Closes: #824324)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Use versioned copyright format URI.
  * Update copyright file header to use current field names (Name =>
    Upstream-Name, Maintainer => Upstream-Contact)
  * Refer to specific version of license GPL-2+.

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 May 2016 01:44:35 +0200

libticket-simple-perl (0.0.2-4.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 17:18:31 +0100

libticket-simple-perl (0.0.2-4) unstable; urgency=medium

  * Team upload

  * Remove perl-modules alternative from libtest-simele-perl build dependency
    (Closes: #788150)

 -- Damyan Ivanov <dmn@debian.org>  Fri, 12 Jun 2015 07:40:44 +0000

libticket-simple-perl (0.0.2-3) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Mark package as autopkgtestable
  * Declare compliance with Debian Policy 3.9.6
  * Add explicit build dependency on libmodule-build-perl
  * mention module name in long description

 -- Damyan Ivanov <dmn@debian.org>  Mon, 08 Jun 2015 10:28:22 +0000

libticket-simple-perl (0.0.2-2) unstable; urgency=low

  * Set the debian perl team as maintainer.
  * Repackage using cdbs and debhelper v6 (not backport-unfriendly v7).
  * Fix FTBFS with newer Module::Build (Closes: #578793).

 -- Xavier Oswald <xoswald@debian.org>  Wed, 19 May 2010 14:17:24 +0200

libticket-simple-perl (0.0.2-1) unstable; urgency=low

  * Initial Release (Closes: #576214).

 -- Xavier Oswald <xoswald@debian.org>  Thu, 01 Apr 2010 17:48:12 +0200
