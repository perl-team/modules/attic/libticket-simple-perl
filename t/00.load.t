use Test::More tests => 1;

BEGIN {
use_ok( 'Ticket::Simple' );
}

diag( "Testing Ticket::Simple  $Ticket::Simple::VERSION" );
