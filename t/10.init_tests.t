#!perl -w

use warnings;
use strict;
use Test::More tests => 5;

BEGIN {
        use_ok( 'Ticket::Simple' );
}

# Test ts creation for Ticket::Simple
my $ts = Ticket::Simple->new();
ok( $ts, '->new returns true' );
ok( ref $ts, '->new returns a reference' );
isa_ok( $ts, 'SCALAR' , '->new returns a hash reference' );
isa_ok( $ts, 'Ticket::Simple', '->new returns a Ticket::Simple object' );
#ok( scalar keys %$ts == 3, '->new returns an object with 3 attributes' );

