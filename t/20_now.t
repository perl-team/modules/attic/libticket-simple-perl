#!perl -w

use warnings;
use strict;
use Test::More tests => 4;
use Time::HiRes qw(gettimeofday);

BEGIN {
    use_ok('Ticket::Simple');
}

my ( $s1, $ms1 ) = gettimeofday;
my $ts = Ticket::Simple->new();
my $n  = $ts->now();
my ( $s2, $ms2 ) = gettimeofday;
ok( $n,              '- now returns true' );
ok( $n > "$s1.$ms1", '- now > before' );
ok( $n < "$s2.$ms2", '- now < after' );

