#!perl -w

use warnings;
use strict;
use Test::More tests => 2;

BEGIN {
    use_ok('Ticket::Simple');
}

my $ts = Ticket::Simple->new();
my ( $t, $v ) = $ts->create_ticket( { login => 't' } );
ok( $ts->store_ticket( { login => 't', ticket => $t, valid => $v } ),
    '- can store ticket' )

