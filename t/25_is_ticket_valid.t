#!perl -w

use warnings;
use strict;
use Test::More tests => 5;

BEGIN {
    use_ok('Ticket::Simple');
}

my $ts = Ticket::Simple->new();
my ( $t1, $v1 ) = $ts->create_ticket( { login => 't' } );
ok( $ts->store_ticket( { login => 't', ticket => $t1, valid => $v1 } ),
    '- can store ticket' );
ok( $ts->is_ticket_valid( { login => 't', ticket => $t1, time => $v1 } ),
    '- is_ticket_valid' );
my $n = $ts->now();
ok( $ts->is_ticket_valid( { login => 't', ticket => $t1, time => $n } ),
    '- is_ticket_valid even now' );
ok(
    (
        not $ts->is_ticket_valid(
            { login => 't', ticket => $t1, time => ( $n + 3000000 ) }
        )
    ),
    '- is_ticket_valid not in teh future'
);

