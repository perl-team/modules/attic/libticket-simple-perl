#!perl -w

use warnings;
use strict;
use Test::More tests => 4;

BEGIN {
    use_ok('Ticket::Simple');
}

my $ts = Ticket::Simple->new( { ttl => 2000 } );
ok( $ts, 'new with ttl param works' );
my $ttl = $ts->get_ttl;
ok( $ttl == 2000, '- got ttl out of the sytsem' );
$ts->set_ttl(3000);
my $ttl2 = $ts->get_ttl;
ok( $ttl2 == 3000, '- got ttl out of the sytsem' );

