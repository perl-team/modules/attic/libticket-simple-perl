#!perl -w

use warnings;
use strict;
use Test::More tests => 8;

BEGIN {
    use_ok('Ticket::Simple');
}

my $ts = Ticket::Simple->new();
my ( $t1, $v1 ) = $ts->create_ticket( { login => 't' } );
ok( $ts->store_ticket( { login => 't', ticket => $t1, valid => $v1 } ),
    '- can store ticket' );
my ( $t2, $v2 ) = $ts->fetch_ticket( { login => 't' } );
ok( $t1 eq $t2, '- ticket are equal' );
ok( $v1 == $v2, '- valid until time is the same' );

ok( $ts->wipe_ticket( { login => 't' } ), '- wipe ticket call ok' );
my ( $t3, $v3 ) = $ts->fetch_ticket( { login => 't' } );
ok( $t3 eq "", '- ticket are equal' );
ok( $v3 == 0, '- valid until time is the same' );
ok(
    (
        not $ts->is_ticket_valid(
            { login => 't', ticket => $t3, time => $v3 }
        )
    ),
    '- is ticket NOT valid'
);

