#!perl -w

use warnings;
use strict;
use Test::More tests => 3;

BEGIN {
    use_ok('Ticket::Simple');
}

my $ts = Ticket::Simple->new();
my ( $t1, $v1 ) = $ts->create_ticket( { login => 't' } );
ok( $ts->store_ticket( { login => 't', ticket => $t1, valid => $v1 } ),
    '- can store ticket' );
ok( $ts->is_ticket_valid_now( { login => 't', ticket => $t1 }),'- is_ticket_valid' );

