use Test::More tests => 1;
use Test::LeakTrace;

no_leaks_ok {
    use Ticket::Simple;
    my $object = Ticket::Simple->new();
}
'no memory leaks';

