use Test::More tests => 2;
use Test::Refcount;

use Ticket::Simple;

my $object = Ticket::Simple->new();

is_oneref( $object, '$object has a refcount of 1' );

my $otherref = $object;

is_refcount( $object, 2, '$object now has 2 references' );

